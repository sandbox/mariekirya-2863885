(function ($) {
  Drupal.behaviors.imageHotspotsAdmin = {
    currentlyActive: 0,

    attach: function (context, settings) {
      $('.position-container', context).hide();

      var $hotspot_fieldsets = $('[data-hotspot-id]', context);

      $hotspot_fieldsets.hide();

      if (this.currentlyActive > 0) {
        var hotspotQuery = '[data-hotspot-id="' + this.currentlyActive + '"]';
        var $active_hotspot = $(hotspotQuery);

        $active_hotspot.show();
      }

      $('.delete-zone', context).hide();
      $('a.admin-hotspot', context).remove();
      $hotspot_fieldsets.each(this.setupHotspots);
    },

    setupHotspots: function() {
      var position = {
        'x': $('.position-x', this).val(),
        'y': $('.position-y', this).val(),
      };

      var hotspot = document.createElement('a');
      hotspot.dataset.id = this.dataset.hotspotId;
      hotspot.className = 'admin-hotspot';
      hotspot.innerHTML = '+';
      hotspot.draggable = true;
      hotspot.style.top = position.y + '%';
      hotspot.style.left = position.x + '%';
      hotspot.onclick = Drupal.behaviors.imageHotspotsAdmin.handleClick;
      hotspot.ondragstart = Drupal.behaviors.imageHotspotsAdmin.handleDragStart;
      hotspot.ondragend = Drupal.behaviors.imageHotspotsAdmin.handleDragEnd;

      $('.hotspot-container').append(hotspot);
    },

    handleClick: function () {
      var $hotspot_fieldsets = $('[data-hotspot-id]');
      $hotspot_fieldsets.hide();

      Drupal.behaviors.imageHotspotsAdmin.currentlyActive = this.dataset.id;

      var hotspotQuery = '[data-hotspot-id="' + this.dataset.id + '"]';
      var $active_hotspot = $(hotspotQuery);

      $active_hotspot.show();
    },

    handleDragStart: function (event) {
      event.dataTransfer.dropEffect = "move";

      $('.delete-zone').show();
    },

    handleDragEnd: function (event) {
      var hotspotSettingsKey = $(this).parent()[0].dataset.hotspotSettings;
      var settings = Drupal.settings[hotspotSettingsKey];

      $('.delete-zone').hide();

      var containerSize = {
        'width': $('.hotspot-container').width(),
        'height': $('.hotspot-container').height()
      };

      var originalHotspotPosition = $(this).position();
      var newHotspotPosition = {
        'top': originalHotspotPosition.top + event.offsetY - 30,
        'left': originalHotspotPosition.left + event.offsetX
      };

      var hotspotPercentPosition = {
        'top': ( newHotspotPosition.top / containerSize.height ) * 100,
        'left': ( newHotspotPosition.left / containerSize.width ) * 100
      };

      if (hotspotPercentPosition.top > 100) {
        hotspotPercentPosition.top = 100;
        $(this).hide();
      }
      else if (hotspotPercentPosition.top > settings.max_y) {
        hotspotPercentPosition.top = settings.max_y;
      }

      if (hotspotPercentPosition.left > settings.max_x) {
        hotspotPercentPosition.left = settings.max_x;
      }

      if (hotspotPercentPosition.top < settings.min_y) {
        hotspotPercentPosition.top = settings.min_y;
      }

      if (hotspotPercentPosition.left < settings.min_x) {
        hotspotPercentPosition.left = settings.min_x;
      }

      var hotspotQuery = '[data-hotspot-id="' + this.dataset.id + '"]';
      var $active_hotspot = $(hotspotQuery);

      $('.position-x', $active_hotspot).val(parseInt(hotspotPercentPosition.left));
      $('.position-y', $active_hotspot).val(parseInt(hotspotPercentPosition.top));

      this.style.left = hotspotPercentPosition.left + '%';
      this.style.top = hotspotPercentPosition.top + '%';
    }
  };
}(jQuery));
