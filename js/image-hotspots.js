(function ($) {
  Drupal.behaviors.imageHotspots = {
    attach: function (context, settings) {
      $('.image-hotspot', context).click(
        function (event) {
          $('.image-hotspot-content').hide();

          var hotspotQuery = 'div.image-hotspot-content[data-id="' + this.dataset.id + '"]';

          $(hotspotQuery).show();
        }
      );

      $('.image-hotspot-close', context).click(
        function (event) {
          $('.image-hotspot-content').hide();
        }
      );
    }
  };
}(jQuery));
