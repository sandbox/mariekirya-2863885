Image Hotspots
==============

Purpose
-------

This module provides an extensible way to allow images to have buttons that
trigger overlays. It supplies a field type that can be added anywhere you can
add and render fields.

Link Types
----------

To create your own type of link you will need to register it with a hook and
create your form and display functions.

Limitations
-----------

- Cardinality on the field must be one due to AJAX interface design. New
  instances of this field would not have a ID associated with it, so for
  now the field uses the machine name and the entity_id to reference
  it's hotspots.